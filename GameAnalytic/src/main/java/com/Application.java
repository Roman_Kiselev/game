package com;

import com.conf.ConfigurationBean;
import com.dao.AnalyticDao;
import com.model.User;
import com.model.UserStatistic;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Application {
    public static void main(String[]arg){
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
                ConfigurationBean.class);
        AnalyticDao dao = ctx.getBean(AnalyticDao.class);
    }
}
