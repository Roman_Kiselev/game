package com.dao;

import com.model.User;
import com.model.UserStatistic;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface AnalyticDao {
    List<User> getUsers(int count);
    Map<String, Long> getUserCount(Date begin, Date end);
    List<UserStatistic> getStatistic(String uuid, Date begin, Date end);
}
