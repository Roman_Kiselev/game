package com.dao.impl;

import com.dao.AnalyticDao;
import com.model.User;
import com.model.UserStatistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AnalyticDaoImpl implements AnalyticDao {
    private final DataSource dataSource;

    @Autowired
    public AnalyticDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<User> getUsers(int count) {
        String query = "select * \n" +
                "from \"user\" s1\n" +
                "where money in (select money\n" +
                "\tfrom \"user\" s2 \n" +
                "\twhere s1.country = s2.country\n" +
                "\torder by money desc\n" +
                "\tlimit ?\n" +
                ")\n";
        JdbcTemplate template = new JdbcTemplate(dataSource);
        List<User> result = template.query(query, new Object[]{count}, (rs, rowNum) ->
                new User(rs.getString("uuid"), rs.getInt("money"),
                        rs.getString("country"), rs.getDate("creationDate")));
        return result;
    }

    @Override
    public Map<String, Long>getUserCount(Date begin, Date end) {
        String query = "select count(uuid), country from public.user where \"creationDate\" between " +
                "? and ? group by country;";
        JdbcTemplate template = new JdbcTemplate(dataSource);
        List<Map<String, Object>> maps = template.queryForList(query, begin, end);
        Map<String, Long> result = new HashMap<>();
        for (Map m : maps){
            result.put((String)m.get("country"), (Long)m.get("count"));
        }
        return result;
    }

    @Override
    public List<UserStatistic> getStatistic(String uuid, Date begin, Date end) {
        String query = "select activity, \"creationDate\" from statistic  where uuid = ? and \"creationDate\" " +
                "between ? and ? order by \"creationDate\";";

        JdbcTemplate template = new JdbcTemplate(dataSource);
        return template.query(query, new Object[]{uuid, begin, end}, (rs, rowNum) ->
                new UserStatistic(rs.getInt("activity"), rs.getDate("creationDate")));
    }
}
