package com.model;

import java.util.Date;

public class User {
    private String uuid;
    private Integer money;
    private String country;
    private Date creationTime;

    public User(String uuid, Integer money, String country, Date creationTime) {
        this.uuid = uuid;
        this.money = money;
        this.country = country;
        this.creationTime = creationTime;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
