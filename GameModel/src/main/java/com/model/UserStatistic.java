package com.model;

import java.util.Date;

public class UserStatistic {
    private Integer activity;
    private String uuid;
    private Date creationDate;

    public UserStatistic() {
    }

    public UserStatistic(Integer activity, Date creationDate) {
        this.activity = activity;
        this.creationDate = creationDate;
    }

    public UserStatistic(Integer activity, String uuid, Date creationDate) {
        this.activity = activity;
        this.uuid = uuid;
        this.creationDate = creationDate;
    }

    public Integer getActivity() {
        return activity;
    }

    public void setActivity(Integer activity) {
        this.activity = activity;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
