package com;

import com.conf.ConfigurationBean;
import com.controller.MoneyController;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] arg){
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
                ConfigurationBean.class);
        ctx.getBean(MoneyController.class).serve();
    }
}
