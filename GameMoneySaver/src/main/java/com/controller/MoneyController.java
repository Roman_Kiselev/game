package com.controller;

import com.dao.MoneyDao;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.InputUserData;
import com.response.ResponseError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import static spark.Spark.post;
import static spark.SparkBase.setPort;

@Controller
public class MoneyController {
    private final MoneyDao moneyDao;

    @Autowired
    public MoneyController(MoneyDao moneyDao) {
        this.moneyDao = moneyDao;
    }

    public void serve() {
        setPort(9081);
        post("/api/userData", (request, response) -> {
            JsonFactory factory = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper(factory);
            InputUserData data;
            JsonNode rootNode;
            Integer money = null;
            try {
                data = mapper.readValue(request.body(), InputUserData.class);
                if (data != null && data.getJson() != null && data.getJson().length() > 0) {
                    rootNode = mapper.readTree(data.getJson());
                    Iterator<Map.Entry<String, JsonNode>> fieldsIterator = rootNode.fields();
                    while (fieldsIterator.hasNext() && money == null) {
                        Map.Entry<String, JsonNode> field = fieldsIterator.next();
                        if (field.getKey().equalsIgnoreCase("money")) {
                            money = field.getValue().intValue();
                        }
                    }
                }
            } catch (IOException e) {
                response.status(500);
                return new ResponseError(e);
            }
            response.type("application/json");
            if (data != null && data.getUuid() != null && money != null) {
                try {

                    return "{\"received\": true,  \"saved\":" + (moneyDao.saveUserMoney(data.getUuid(), money) != 0) + "}";
                } catch (Exception e) {
                    response.status(500);
                    return new ResponseError(e);
                }
            }else {
                return "{\"received\": true, \"saved\":false}";
            }
        });
    }
}
