package com.dao;

public interface MoneyDao {
    int saveUserMoney(String uuid, Integer money) throws Exception;
}
