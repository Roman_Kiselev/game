package com.dao.impl;

import com.dao.MoneyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class MoneyDaoImpl implements MoneyDao {
    private final DataSource dataSource;

    @Autowired
    public MoneyDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public int saveUserMoney(String uuid, Integer money) {
        String sql = "update \"user\" set money  = ? where uuid = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.update(sql, money, uuid);
    }
}
