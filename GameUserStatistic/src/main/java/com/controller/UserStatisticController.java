package com.controller;

import com.dao.UserStatisticDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.UserStatistic;
import com.response.ResponseError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.IOException;

import static spark.Spark.get;
import static spark.Spark.setPort;


@Controller
public class UserStatisticController {
    private final UserStatisticDao userStatisticDao;

    @Autowired
    public UserStatisticController(UserStatisticDao userStatisticDao) {
        this.userStatisticDao = userStatisticDao;
    }
    public void serve() {
        setPort(9082);

        get("/api/userData/:uuid", ((request, response) -> {
            try {
            UserStatistic lastUserStat = userStatisticDao.getLastUserStat(request.params(":uuid"));
            response.type("application/json");
            ObjectMapper mapper = new ObjectMapper();
                return mapper.writeValueAsString(lastUserStat);
            } catch (IOException e) {
                System.out.println(e.getMessage());
                response.status(500);
                return new ResponseError(e);
            }
        }));
    }
}
