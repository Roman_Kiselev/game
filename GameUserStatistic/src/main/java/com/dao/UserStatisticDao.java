package com.dao;

import com.model.UserStatistic;

public interface UserStatisticDao {
    UserStatistic getLastUserStat(String uuid);
}
