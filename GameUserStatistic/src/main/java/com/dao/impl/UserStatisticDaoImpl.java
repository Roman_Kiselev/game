package com.dao.impl;

import com.dao.UserStatisticDao;
import com.model.UserStatistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class UserStatisticDaoImpl implements UserStatisticDao {
    private final DataSource dataSource;

    @Autowired
    public UserStatisticDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public UserStatistic getLastUserStat(String uuid) {
        String sql = "select * from \"statistic\" where uuid = ? order by \"creationDate\" limit 1;";
        try(PreparedStatement stmt = dataSource.getConnection().prepareStatement(sql)){
            stmt.setString(1, uuid);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()){
                    return new UserStatistic(
                            rs.getInt("activity"), rs.getString("uuid"),
                            rs.getDate("creationDate"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
