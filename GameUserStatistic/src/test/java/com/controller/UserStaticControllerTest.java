package com.controller;


import com.Application;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.UserStatistic;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import spark.Spark;
import spark.utils.IOUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.*;


public class UserStaticControllerTest {

    @BeforeClass
    public static void beforeClass() {
        Application.main(null);
    }

    @AfterClass
    public static void afterClass() {
        Spark.stop();
    }

    @Test
    public void noUserTest() {
        TestResponse res = request("GET", "/api/userData/noDataUser");
        assertNotNull(res);
        assertEquals(200, res.status);
        assertNotNull(res.body);
        assertEquals("null", res.body);
    }

    @Test
    public void userExist() throws IOException {
        TestResponse res = request("GET", "/api/userData/sadasdasdmasl;asd");
        assertNotNull(res);
        assertEquals(200, res.status);
        assertNotNull(res.body);
        ObjectMapper mapper = new ObjectMapper();
        UserStatistic userStatistic = mapper.readValue(res.body, UserStatistic.class);
        assertNotNull(userStatistic);
        assertTrue(userStatistic.getActivity().equals(123));
        assertTrue(userStatistic.getUuid().equals("sadasdasdmasl;asd"));
    }

    private TestResponse request(String method, String path) {
        try {
            URL url = new URL("http://localhost:9082" + path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.connect();
            String body = IOUtils.toString(connection.getInputStream());
            return new TestResponse(connection.getResponseCode(), body);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Sending request failed: " + e.getMessage());
            return null;
        }
    }

    private static class TestResponse {
        final String body;
        final int status;

        TestResponse(int status, String body) {
            this.status = status;
            this.body = body;
        }
    }

}
