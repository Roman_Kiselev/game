CREATE TABLE "statistic"
(
  id bigint NOT NULL,
  uuid text,
  activity integer,
  "creationDate" timestamp
);

CREATE TABLE "user"
(
  uuid text NOT NULL,
  country text,
  "creationDate" timestamp,
  money integer
);