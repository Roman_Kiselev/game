package com.controller;

import com.dao.UserStatisticSaverDao;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.UserStatistic;
import com.response.ResponseError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import static spark.Spark.post;
import static spark.SparkBase.setPort;

@Controller
public class UserStatisticSaverController {
    private final UserStatisticSaverDao userStatisticSaverDao;

    @Autowired
    public UserStatisticSaverController(UserStatisticSaverDao userStatisticSaverDao) {
        this.userStatisticSaverDao = userStatisticSaverDao;
    }

    public void serve() {
        setPort(9083);

        post("/api/usersStatistic", (request, response) -> {
            JsonFactory factory = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper(factory);
            UserStatistic stat;
            response.type("application/json");
            try {
                stat = mapper.readValue(request.body(), UserStatistic.class);
                if (stat != null && stat.getUuid() != null && stat.getActivity() != null) {
                    userStatisticSaverDao.saveUserStatistic(stat.getUuid(), stat.getActivity());
                    return "{\"received\": true, \"saved\":true}";
                }
            } catch (Exception e) {
                response.status(500);
                return new ResponseError(e);
            }
            return "{\"received\": true, \"saved\":false}";
        });
    }
}
