package com.dao;

public interface UserStatisticSaverDao {
    void saveUserStatistic(String uuid, Integer activity) throws Exception;
}
