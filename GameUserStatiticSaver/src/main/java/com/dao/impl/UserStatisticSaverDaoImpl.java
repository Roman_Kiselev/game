package com.dao.impl;

import com.dao.UserStatisticSaverDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Date;

@Repository
public class UserStatisticSaverDaoImpl implements UserStatisticSaverDao {
    private final DataSource dataSource;

    @Autowired
    public UserStatisticSaverDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void saveUserStatistic(String uuid, Integer activity) {
        String sql = "insert into \"statistic\" (uuid, activity, \"creationDate\") values (?,?,?);";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(sql, uuid, activity, new Date());
    }
}
