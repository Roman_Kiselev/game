CREATE SEQUENCE public.statistic_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 7
  CACHE 1;
ALTER TABLE public.statistic_seq
  OWNER TO postgres;

  CREATE TABLE public."user"
(
  uuid text NOT NULL,
  country text,
  "creationDate" date,
  money integer,
  CONSTRAINT uuid_pkey PRIMARY KEY (uuid)
);

CREATE INDEX user_creation_date_index
  ON public."user"
  USING btree
  ("creationDate");
  
  CREATE TABLE public.statistic
(
  id bigint NOT NULL DEFAULT nextval('statistic_seq'::regclass),
  uuid text,
  activity integer,
  "creationDate" timestamp without time zone,
  CONSTRAINT statistic_id_pkey PRIMARY KEY (id),
  CONSTRAINT statistic_user_uuid_skey FOREIGN KEY (uuid)
      REFERENCES public."user" (uuid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE INDEX creation_date_statistic_index
  ON public.statistic
  USING btree
  ("creationDate");

CREATE INDEX statistic_uuid_index
  ON public.statistic
  USING btree
  (uuid COLLATE pg_catalog."default");



